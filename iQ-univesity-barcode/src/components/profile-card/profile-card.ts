import { Component, Input, ElementRef, Renderer } from '@angular/core';
import { UtilProvider } from '../../providers/util-provider';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import * as $ from 'jquery';

@Component({
  selector: 'profile-card',
  templateUrl: 'profile-card.html'
})

export class ProfileCardComponent {
  @Input('persona') persona: any;
  @Input('allowChangePicture') allowChangePicture: boolean;

  folder: string = 'users';
  constructor(
    private el: ElementRef,
    private renderer: Renderer,
    public afoDatabase: AngularFireOfflineDatabase,
    private util: UtilProvider
  ) { }

  ngOnChanges() {
    this.allowChangePicture = !!this.allowChangePicture;
    if (!this.util.isEmpty(this.persona.picture || this.persona.image)) {
      this.persona.picture = this.persona.picture ? this.persona.picture : this.persona.image;
      this.renderer.setElementStyle(this.el.nativeElement.firstChild, 'backgroundImage', 'url(' + this.persona.picture + ')');
    }
  }

  changePicture() {
    if (!this.allowChangePicture) {
      return false;
    }
    $('#attachProfilePicture').click();
  }

  getClass() {
    return this.allowChangePicture ? 'ion-avatar-upload-image' : '';
  }

  saveUrl(res: any) {
    this.persona.picture = res.url;
    this.afoDatabase
      .object('users/' + this.persona.uid + '/picture')
      .set(res.url);
  }

  name() {
    if (this.persona.name) {
      return this.persona.name;
    }
    else if(this.persona.Name){
      return this.persona.Name;
    }
    return (this.persona.firstName || '') + ' ' + (this.persona.lastName || '')
  }
}
