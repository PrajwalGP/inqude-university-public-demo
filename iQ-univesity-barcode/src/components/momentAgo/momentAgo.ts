import {Component, Input} from '@angular/core';
import moment from 'moment';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'moment-ago',
  templateUrl: 'momentAgo.html'
})
export class MomentAgo {
  @Input('timeStamp') timeStamp: any;
  @Input('params') params: any;
  timeAgo: any;
  subscription: any;
  constructor() { }

  ngOnChanges() {
    this.timeAgo = moment(+ this.timeStamp).fromNow();
    this.subscription = Observable.interval(4000).subscribe(x => {
      this.timeAgo = moment(+ this.timeStamp).fromNow();
    });
  }

  // kill interval if routing is changed
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
