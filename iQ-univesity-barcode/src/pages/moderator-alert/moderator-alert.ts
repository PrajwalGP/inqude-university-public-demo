import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

@Component({
  selector: 'moderator-alert',
  templateUrl: 'moderator-alert.html'
})

export class ModeratorAlert {
  alertData: any;
  eventName: any;
  isEventViewable: boolean;
  constructor(
    public view: ViewController,
    public params: NavParams
  ) {
    this.alertData = params.get('data');
    this.isEventViewable = this.alertData.enableEventView == 'true' ? true : false;
  }

  dismiss() {
    this.view.dismiss('DISMISS');
  }

  showEvent() {
    this.view.dismiss('GOTO_EVENT')
  }
}
