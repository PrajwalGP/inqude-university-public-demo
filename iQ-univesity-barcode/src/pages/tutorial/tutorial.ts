import { Component } from '@angular/core';
import { MenuController, NavController, Slides } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';
import { NavigationDashboardPage } from '../navigation-dashboard/navigation-dashboard';
import { AngularFire } from 'angularfire2';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { HelperService } from '../../providers/helperService';
import { StatusBar } from '@ionic-native/status-bar';
import moment from 'moment';

@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
  providers: [HelperService]
})

export class TutorialPage {
  showSkip = true;
  HAS_LOGGED_IN: any;
  screens: any = { screen1: '', screen2: '', screen3: '' };
  statusBars: any = ['#000', '#000', '#000', '#488aff'];
  constructor(
    public af: AngularFire,
    public afoDatabase: AngularFireOfflineDatabase,
    public navCtrl: NavController,
    public menu: MenuController,
    public storage: Storage,
    public helper: HelperService,
    private statusBar: StatusBar
  ) { }

  startApp() {
    this.statusBar.backgroundColorByHexString(this.statusBars[3]);
    // if user has loggedin, show HomePage else show LoginPage
    if (this.HAS_LOGGED_IN == true) {
      this.navCtrl.setRoot(NavigationDashboardPage)
    } else {
      this.navCtrl.setRoot(LoginPage)
        .then(() => {
          this.statusBar.backgroundColorByHexString('#488aff');
          return this.storage.set('hasSeenTutorial', 'true');
        })
        .then(() => {
          return this.storage.get('appOpenedDate')
        })
        .then((appOpenedDate) => {
          if (appOpenedDate == null) {
            this.storage.set('appOpenedDate', +(moment.utc().format('x')))
          }
        })
    }
  }

  onSlideDidChange(event: Slides) {
    this.statusBar.backgroundColorByHexString(this.statusBars[event._activeIndex]);
  }

  onSlideChangeStart(event: Slides) {
    this.showSkip = !event.isEnd();
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
    // for slide-1
    this.statusBar.backgroundColorByHexString(this.statusBars[0]);

    // check if user has loggedIn or not
    this.storage.get("hasLoggedIn")
      .then((value) => {
        this.HAS_LOGGED_IN = value;
      });

    this.afoDatabase.object('/settings/welcomeScreen', { preserveSnapshot: true })
      .take(1)
      .subscribe((screens: any) => {
        this.screens = screens;
      })
  }

  ionViewDidLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }
}
