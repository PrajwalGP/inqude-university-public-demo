import { Component } from '@angular/core';
import { LoadingController, MenuController, ModalController, NavController } from 'ionic-angular';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { SendNotificationsPage } from '../send-notifications/send-notifications';
import { NotificationDetailPage } from '../notification-detail/notification-detail';
import { FirebaseAnalyticsProvider } from '../../../providers/firebase-analytics-provider';
import { UserData } from '../../../providers/user-data';
import { HelperService } from '../../../providers/helperService';
import { ModeratorList } from '../../moderator-list/moderator-list';
import { EventDetailPage } from '../../event-detail/event-detail';
import { Talisma } from '../../../providers/crm/talisma';
import * as _ from 'underscore';

@Component({
  selector: 'page-notifications',
  templateUrl: 'notification-list.html',
  providers: [FirebaseAnalyticsProvider]
})

export class NotificationsListPage {
  notifications: any = [];
  sentNotifications: any = [];
  receivedNotifications: any = [];
  notificationType: string = "SENT";
  uid: any;
  roleValue: any;

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public firebaseAnalytics: FirebaseAnalyticsProvider,
    public helper: HelperService,
    public loadingCtrl: LoadingController,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public talisma: Talisma,
    public userData: UserData
  ) {
    this.menuCtrl.enable(true);
    let loading = this.loadingCtrl.create();
    loading.present();
    this.firebaseAnalytics.setScreen('NOTIFICATIONS');
    let self:any = this;
    //enabling the sidemenu with swipe
    this.userData.getUid()
      .then((uid) => {
        this.uid = uid;
        this.userData.getRoleValue()
          .then((roleValue) => {
            this.roleValue = roleValue;
            if (roleValue == 99) {
              this.afoDatabase.list('/eventNotifications', { query: { orderByChild: 'createdBy', equalTo: this.uid } })
                .subscribe((res) => {
                  this.afoDatabase.object('/users/')
                    .take(1)
                    .subscribe((users) => {
                      let notifications = res.length > 0 ? _.sortBy(res, function(obj: any) { return +(obj.createdDate) }).reverse() : [];
                      this.notifications = _.each(notifications, function(eachNotification: any) {
                        let temp: any = [];

                        if (typeof (eachNotification.targetUsers) == 'object') {
                          (eachNotification.targetUsers).forEach((user: any) => {
                            if (users[user]) {
                              temp.push(users[user].firstName + ' ' + users[user].lastName + ', ');
                            }
                          })
                          if (temp.length > 1) {
                            temp[temp.length - 1] = temp[temp.length - 1].replace(',', '');
                            temp[temp.length - 2] = temp[temp.length - 2].replace(',', ' and ');
                            temp.push('are');
                          }
                          else if (temp.length == 1) {
                            temp[temp.length - 1] = temp[temp.length - 1].replace(',', '');
                            temp.push('is');
                          }
                        }

                        if (eachNotification.body.indexOf('You have been') > -1) {
                          eachNotification.body = eachNotification.message.replace("You have been", temp.join('')) + ' - \"' + JSON.parse(eachNotification.event).eventName + '\"';
                        }
                        return true;
                      })
                      loading.dismiss();
                    })

                });
            } else {
              this.afoDatabase.object('/users/' + this.uid)
                .take(1)
                .subscribe((userData) => {
                  this.afoDatabase.list('/eventNotifications')
                    .subscribe((res) => {
                      loading.dismiss();
                      self.notifications = this.sentNotifications = _.filter(res, function(obj: any) { return ((obj.createdBy == uid) && (obj.recieverApp == 'Student') && (obj.senderType == 'Moderator')) });
                      self.receivedNotifications = _.filter(res, function(obj: any) { return ((obj.createdBy != uid && Object.keys(userData.notifications || []).indexOf(obj.id) > -1) && ((obj.recieverApp == 'Moderator' && self.roleValue == 20 && obj.senderType == 'Admin'))); });
                    });
                })
            }
          })
          .catch((err) => {
            console.log('err occured', err);
          })
      })
      .catch((err) => {
        console.log('err occured', err);
      })
  }

  viewNotification(notification: any) {
    if (this.roleValue == 20 && notification.target == 'Event Notification') {
      notification.event = (notification.event && typeof(notification.event) == 'string') ? JSON.parse(notification.event) : notification.event;
      if (notification.event && notification.event.eventId) {
        this.helper.showEventAlert(notification, notification.enableEventView)
          .then((res: any) => {
            if (res.status != 'DISMISS' && notification.event.eventId) {
              this.helper.showLoading();
              this.talisma.events.getEventById(notification.event.eventId)
                .then((res: any) => {
                  let event = res[0];
                  this.helper.hideLoading();
                  this.navCtrl.setPages([{ page: ModeratorList, params: {} }, { page: EventDetailPage, params: { event: event } }])
                })
                .catch((err: any) => {
                  console.log(err);
                  this.helper.hideLoading();
                })
            }
          });
      }
    } else {
      this.navCtrl.push(NotificationDetailPage, {
        notification: notification
      })
    }
  };

  listNotifications(notificationType: any) {
    this.notifications = notificationType == "SENT" ? this.sentNotifications : this.receivedNotifications;
  }

  addNewNotifications() {
    let modal = this.modalCtrl.create(SendNotificationsPage);
    modal.present();
  }
}
