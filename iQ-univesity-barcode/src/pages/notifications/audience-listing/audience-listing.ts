import { Component } from '@angular/core';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { NavParams, ViewController } from 'ionic-angular';
import { UserData } from '../../../providers/user-data';
import * as _ from 'underscore';
import { HelperService } from '../../../providers/helperService';
@Component({
  selector: 'page-audience-listing',
  templateUrl: 'audience-listing.html'
})

export class AudienceListing {
  targets: any[] = [];
  selectedTargets: any[] = [];
  filterKeys: any = ["name"];
  query: any = "";
  pastTargets: any = [];
  liveTargets: any = [];
  loadedTargets: any = [];
  segment: string = 'live';
  allEvents: any[] = [];
  roleValue: any;
  universityRef: any = '';

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public helperService: HelperService,
    public navParams: NavParams,
    public userData: UserData,
    public viewCtrl: ViewController
  ) {
    this.userData.getRoleValue()
      .then((roleValue) => {
        this.roleValue = roleValue;
        return this.userData.getUniversityId();
      })
      .then((universityID) => {
        this.universityRef = 'universitiesData/' + universityID + '/';
        afoDatabase.list(this.universityRef + 'notificationTargets')
          .subscribe((snapshots) => {
            if (this.roleValue == 99) {
              this.targets = snapshots;
              this.getTargetEvents();
            }
            else {
              this.userData.getUid()
                .then((uid) => {
                  this.afoDatabase.object(this.universityRef + 'moderatorEvents/' + uid)
                    .subscribe((response) => {
                      delete response.$key;
                      delete response.$exists;
                      this.targets = [];
                      Object.keys(response).forEach((eventId: any) => {
                        let index = _.findIndex(snapshots, (snapshot) => {
                          return snapshot.eventId == +eventId || snapshot.eventId == eventId;
                        });
                        if (index > -1) {
                          this.targets.push(snapshots[index]);
                        }
                      })
                      this.getTargetEvents();
                    })
                })
            }
          });
      })
  }

  getTargetEvents() {
    let self: any = this;
    this.getAllEvents()
      .then(() => {
        self.targets.forEach((target: any) => {
            target['event'] = this.allEvents[target.eventId] || {};
        })
        let temp = _.partition(self.targets, function(target: any) {
          if (target.event.EventStartDate >= self.helperService.getTimeStamp()) {
            return target.event.EventId;
          }
        });
        this.loadedTargets = this.segment == 'live' ? temp[0] : temp[1];
      })
  }

  getAllEvents() {
    return new Promise((resolve, reject) => {
      if (this.allEvents && this.allEvents.length) {
        resolve();
      }
      this.afoDatabase.object(this.universityRef + 'events/')
        .take(1)
        .subscribe((response: any) => {
          this.allEvents = response;
          resolve();
        })
    })
  }

  insertTargetToArray(data: any) {
    let index = _.findIndex(this.selectedTargets, { eventId: data.eventId });
    if (index > -1) {
      this.selectedTargets.splice(index, 1);
    }
    else {
      this.selectedTargets.push(data);
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  updateList() {
    this.getTargetEvents();
  }

  continue() {
    this.viewCtrl.dismiss(this.selectedTargets);
  }
}
