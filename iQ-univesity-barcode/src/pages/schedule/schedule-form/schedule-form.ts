import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { App, AlertController, LoadingController, ModalController, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { AddSpeakersPage } from '../add-speakers/add-speakers';
import { FileUploader } from '../../file-uploader/file-uploader';
import { HelperService } from '../../../providers/helperService';
import { AngularFire } from 'angularfire2';
import * as _ from 'underscore';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import * as moment from 'moment-timezone';
import { UserData } from '../../../providers/user-data';
import { DateTime } from 'ionic-angular'
@Component({
  selector: 'page-schedule-form',
  templateUrl: 'schedule-form.html'
})

export class ScheduleFormPage {
  session?: any = {};
  submitted = false;
  edit: any;
  personas: any = [];
  tracks?: any[] = [];
  personaIds: any = {};
  personaType: any = [];
  minDate: string;
  maxDate: string;
  timeZone: any;
  splicedPersonaIds: any[] = [];
  universityRef: any = '';
  @ViewChild('datePicker') datePicker: DateTime;
  constructor(
    public af: AngularFire,
    public afoDatabase: AngularFireOfflineDatabase,
    public alertCtrl: AlertController,
    public app: App,
    public helperService: HelperService,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public userData: UserData,
    public viewCtrl: ViewController
  ) {
    this.userData.getUniversityId()
      .then((universityId) => {
        this.universityRef = 'universitiesData/' + universityId
        this.edit = navParams.get('EventId') ? true : false;
        if (navParams.get('startTime')) {
          this.session.startTime = navParams.get('startTime');
          this.session.endTime = navParams.get('startTime');
          this.session.images = this.session.images || [];
          this.session.files = this.session.files || [];
        }

        // dynamic schedule dates
        this.afoDatabase.list(this.universityRef + '/settings/eventDate')
          .subscribe((dates) => {
            if (this.edit !== true) {
              this.minDate = moment().format('YYYY-MM-DD');
            }
            else {
            }
            this.maxDate = moment(dates[dates.length - 1].$value).format('YYYY-MM-DD');
          })

        // Get all personas
        this.afoDatabase.list(this.universityRef + '/speakers', { preserveSnapshot: true })
          .subscribe((personas) => {
            this.personas = _.uniq(Object.assign([], _.sortBy(personas, "Name")), function(persona) {
              return persona.id;
            });
          });


        this.afoDatabase.object(this.universityRef + '/settings/timeZone')
          .subscribe((timeZone) => {
            this.timeZone = timeZone.$value;
            // pre-fill sesion details
            if (this.edit === true) {
              this.af.database.object(this.universityRef + '/events/' + navParams.get('EventId'), { preserveSnapshot: true })
                .subscribe((session) => {
                  this.session = session.val();
                  if (this.session) {
                    this.session.images = this.session && this.session.images ? this.session.images : [];
                    this.session.TotalSeats = this.session && this.session.TotalSeats ? this.session.TotalSeats : 0;
                    this.session.RegisteredParticipants = this.session && this.session.RegisteredParticipants ? this.session.RegisteredParticipants : 0;
                    this.session.AttendedParticipants = this.session && this.session.AttendedParticipants ? this.session.AttendedParticipants : 0;
                    this.session.EventStartDate = moment(this.session.EventStartDate).tz(this.timeZone).format('');
                    this.session.EventEndDate = moment(this.session.EventEndDate).tz(this.timeZone).format('');
                    _.defaults(this.session, { personaIds: {} })
                    this.personaType = Object.keys(this.session.personaIds);
                    this.personaIds = this.session.personaIds;
                    this.session.files = this.session && this.session.files ? this.session.files : [];
                  }
                });
            }
            else {
              this.session.EventStartDate = moment().tz(this.timeZone).format('');
              this.session.EventEndDate = moment().tz(this.timeZone).add(5, 'minutes').format('');
            }
          })
      })
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onSubmit(form: NgForm) {
    let loading = this.loadingCtrl.create({
      content: 'Saving please wait...'
    })
    loading.present();
    this.submitted = true;
    let childRef: any;
    _.defaults(this.personaIds, {})
    let session: any = _.clone(this.session);
    if (this.edit == true && session.TotalSeats < session.RegisteredParticipants) {
      loading.dismiss();
      this.alertCtrl.create({
        title: 'Please enter valid Total seats count',
        message: 'Total seats should be greater than the no. of registered participants : ' + session.RegisteredParticipants,
        buttons: [
          {
            text: 'Ok',
          }
        ]
      }).present();
      // this.helperService.showMessage('TotalSeats seats cannot be less than the Registered participants: ' + );
      return false;
    }
    // set session time w.r.t timeZone defined
    session.EventEndDate = moment.utc(session.EventEndDate).format('');
    session.EventStartDate = moment.utc(session.EventStartDate).format('');
    session.RegisteredParticipants = session.RegisteredParticipants || 0;
    session.AttendedParticipants = session.AttendedParticipants || 0;
    session.TotalSeats = session.TotalSeats || 0;
    if (form.valid) {
      session.personaIds = this.personaIds;
      // session.tracks = this.tracks;
      // session.address = session.address ? session.address.trim() : '';
      session.images = session.images || [];
      if (session.images[0]) {
        session.EventImage = session.images[0];
      } else {
        // delete session.EventImage;
      }
      if (this.edit !== true) {
        childRef = this.af.database.list(this.universityRef + '/events').push({});
        session.EventId = childRef.key;
      }
      this.linkPersonaToSession(session)
        .then(() => {
          if (this.edit == true) {
            this.unlinkPersonaFromSession(session)
              .then(() => {
                this.af.database.object(this.universityRef + '/events/' + session.EventId)
                  .set(session)
                  .then(() => {
                    loading.dismiss();
                    this.viewCtrl.dismiss();
                  })
                  .catch((err) => {
                    console.log('failed to edit a session', err);
                  });
              })
              .catch((err) => {
                console.log('err occured', err);
              })

          }
          else {
            childRef.set(session)
              .then(() => {
                loading.dismiss();
                this.viewCtrl.dismiss();
              })
              .catch((err: any) => {
                console.log('failed to create a session', err);
              });
          }
        })
        .catch((err) => {
          console.log('err occured', err);
        })
    }
  }

  linkPersonaToSession(session: any) {
    return new Promise((resolve, reject) => {
      Object.keys(session.personaIds).forEach((type: any) => {
        session.personaIds[type].forEach((key: any) => {
          this.af.database.object(this.universityRef + '/speakers/' + key, { preserveSnapshot: true })
            .take(1)
            .subscribe((snapshot) => {
              let speaker = snapshot.val();
              _.defaults(speaker, { sessionIds: [] });
              let index = speaker.sessionIds.indexOf(session.EventId);
              if (index < 0) {
                speaker.sessionIds.push(session.EventId)
                this.af.database.object(this.universityRef + '/speakers/' + key)
                  .set(speaker);
              }
            })
        })
      })
      resolve();
    })
  }

  unlinkPersonaFromSession(session: any) {
    return new Promise((resolve, reject) => {
      if (this.splicedPersonaIds && this.splicedPersonaIds.length > 0) {
        this.splicedPersonaIds.forEach((id) => {
          this.af.database.object(this.universityRef + '/speakers/' + id, { preserveSnapshot: true })
            .take(1)
            .subscribe((res) => {
              let speaker = res.val();
              let index = speaker.sessionIds.indexOf(session.EventId);
              if (index > -1) {
                speaker.sessionIds.splice(index, 1);
                this.af.database.object(this.universityRef + '/speakers/' + id)
                  .set(speaker);
              }
            })
        })
        resolve();
      }
      else {
        resolve();
      }
    })
  }

  deleteItem(type: string, index: number) {
    //deletes the speaker or tracks
    if (type === 'track') {
      this.tracks.splice(index, 1);
    }
  }

  checkFormValid(form: NgForm) {
    //checks whether the form is valid or not
    return !(form.valid);
  }

  isExists(type: any) {
    return this.personaIds[type] && this.personaIds[type].length;
  }

  addFiles() {
    let self = this;
    let modal = this.modalCtrl.create(FileUploader, { folder: 'schedule', uploadType: 'file', allowMultipleSelection: true })
    modal.present();
    modal.onDidDismiss(function(files: any) {
      if (files) {
        self.session.files = self.session.files || [];
        for (let i = 0; i < files.length; i++) {
          self.session.files.indexOf(files[i]) == -1 ? self.session.files.push(files[i]) : '';
        }
      }
    });
  }

  addPeronas() {
    this.updatePersonas();
    let modal = this.modalCtrl.create(AddSpeakersPage, { personas: this.personas, splicedPersonaIds: this.splicedPersonaIds });
    modal.present();

    modal.onDidDismiss((ids: any, type: any) => {
      let self: any = this;
      if (ids && type && ids.length) {
        self.personaIds[type] = self.personaIds[type] || [];
        self.personaType = Object.keys(self.personaIds);

        // Add personaIds to display que
        self.setIds(ids, type);

        // update persona lists
        self.updatePersonas();
      }
    });
  }

  //Remove Files from the list
  removeFile(file: any) {
    this.session.files.splice(this.session.files.indexOf(file.id), 1);
  }

  // delete personas from selected/existing list
  removePersona(data: any) {
    let alert = this.alertCtrl.create({
      title: 'Confirmation',
      message: 'Are you sure to delete this perosna?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Continue',
          handler: () => {
            return new Promise((resolve, reject) => {
              let personaIds = this.personaIds[data.type];
              for (let m = 0; m < personaIds.length; m++) {
                if (personaIds[m] == data.id) {
                  personaIds.splice(m, 1);
                  this.splicedPersonaIds.push(data.id);
                  resolve();
                }
              }
            }).then(() => {
              new Promise((resolve, reject) => {
                if (this.personaIds[data.type].length == 0) {
                  let index = this.personaType.indexOf(data.type);
                  if (index > -1) {
                    this.personaType.splice(index, 1);
                    resolve();
                  }
                }
              })
            }).then(() => {
              new Promise((resolve, reject) => {
                this.afoDatabase.object(this.universityRef + '/speakers/' + data.id, { preserveSnapshot: true })
                  .take(1)
                  .subscribe((snapshot) => {
                    delete snapshot.$key;
                    delete snapshot.$exists;
                    let persona = _.find(this.personas, ((persona: any) => { return persona.id == snapshot.id; }));
                    if (!persona) {
                      this.personas.push(snapshot);
                    }
                    resolve();
                  });
              })
            });
          }
        }
      ]
    });
    alert.present();
  }

  // update list of personas that are not in selected or saved lists
  updatePersonas() {
    for (let k = 0; k < this.personaType.length; k++) {
      let selectedIds = this.personaIds[this.personaType[k]];
      for (let i = 0; i < selectedIds.length; i++) {
        for (let j = 0; j < this.personas.length; j++) {
          if (this.personas[j].id == selectedIds[i]) {
            this.personas.splice(j, 1);
          }
        }
      }
    }
  }

  // user selected personas ids
  setIds(ids: any, type: any) {
    for (let i = 0; i < ids.length; i++) {
      this.personaIds[type].push(ids[i]);
    }
  }

  changeImage() {
    //changing the image only from the desktop
    let modal = this.modalCtrl.create(FileUploader, { folder: 'schedule' });
    modal.present();

    modal.onDidDismiss((url: any) => {
      if (url) {
        this.session.EventImage = url;
      }
    })
  }

  minStartDate(): string {
    return moment().format('YYYY-MM-DD');
  }

  minEndDate(): string {
    return moment().add(5, 'minutes').format('YYYY-MM-DD');
  }
  // endTimeChanged() {
  //   if (moment(this.session.EventEndDate).isSameOrBefore(this.session.EventStartDate)) {
  //     console.log('before this.session.EventEndDate', this.session.EventEndDate)
  //     let momentObj: any = moment(this.session.EventStartDate).tz(this.timeZone).add(5, 'minutes');
  //     this.session.EventEndDate = momentObj.format('');
  //     this.datePicker._text = momentObj.format('DD-MMM-YYYY hh:mm a');
  //     this.datePicker.updateText();
  //   }
  // }

  endTimeChanged() {

    if (moment(this.session.EventEndDate).isSameOrBefore(this.session.EventStartDate) || (moment.duration(moment(this.session.EventEndDate).diff(moment(this.session.EventStartDate))).as('minutes') <5)) {
      let momentObj: any = moment(this.session.EventStartDate).tz(this.timeZone).add(5, 'minutes');
      this.session.EventEndDate = moment(this.session.EventStartDate).tz(this.timeZone).add(5, 'minutes').format('');
      this.datePicker._text = momentObj.format('DD-MMM-YYYY hh:mm a');
      this.datePicker.setValue(this.session.EventEndDate);
    }
  }

  changeEndDate(): void {
    if (moment(this.session.EventStartDate).isSameOrAfter(this.session.EventEndDate) || (moment.duration(moment(this.session.EventEndDate).diff(moment(this.session.EventStartDate))).as('minutes') <5)) {
      this.session.EventEndDate = moment.utc(this.session.EventStartDate).tz(this.timeZone).add(5, 'minutes').format('')
    }
  }
}
