import { Component, Input } from '@angular/core';

import { PostComponent } from '../../post.component';

@Component({
  templateUrl: 'image-gallery-post.component.html',
  selector: 'component-image-gallery-post'
})

export class ImageGalleryPostComponent implements PostComponent {
  @Input() post: any = [];
  @Input() params: any;
}
