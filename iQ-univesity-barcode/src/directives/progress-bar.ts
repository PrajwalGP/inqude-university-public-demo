import { Directive, Input, ElementRef, Renderer, OnChanges } from '@angular/core';

@Directive({
  selector: '[progress-bar]'
})

export class ProgressBar implements OnChanges {
  constructor(public el: ElementRef, public renderer: Renderer, ) { }
  @Input('value') value: any;
  @Input('total') total: any;
  percentage: any;

  ngOnChanges() {
    this.value = this.value || 0;
    this.total = this.total || 0;
    this.percentage = ((this.value / this.total) * 100);
    let color: string = '#dbdbdb';
    if (this.percentage < 25) {
      color = '#ffdb76';
    } else if (this.percentage < 50) {
      color = '#ffc800';
    } else if (this.percentage < 75) {
      color = '#f90';
    } else if (this.percentage > 75) {
      color = '#69ce2b';
    }
    this.renderer.setElementStyle(this.el.nativeElement, 'width', this.percentage + '%');
    this.renderer.setElementStyle(this.el.nativeElement, 'padding', '0');
    this.renderer.setElementStyle(this.el.nativeElement, 'backgroundColor', color);
  }
}
