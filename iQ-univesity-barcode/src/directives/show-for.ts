import { Directive, Input, ElementRef, Renderer } from '@angular/core';
import { Storage } from '@ionic/storage';

@Directive({
  selector: '[showFor]'
})

export class ShowForDirective {
  currUserRole: any;
  constructor(
    public el: ElementRef,
    public renderer: Renderer,
    public storage: Storage
  ) {
  }
  @Input('showFor') roles: string;

  // 10 - GUEST
  // 0 - USER
  // 20 - MODERATOR
  // 99 - ADMINISTRATOR

  ngOnChanges() {
    if (this.roles) {

      let roleArr = this.roles.split(',');

      for (let i = 0; i < roleArr.length; i++) {
        if (roleArr[i] == 'admin') {
          roleArr[i] = '99';
        } else if (roleArr[i] == 'registered') {
          roleArr[i] = '0';
        } else if (roleArr[i] == 'moderator') {
          roleArr[i] = '20';
        }
        else {
          roleArr[i] = '10';
        }
      }

      this.storage.get('roleValue').then((value) => {
        this.currUserRole = value !== null ? value.toString() : '10';

        let pos = roleArr.indexOf(this.currUserRole);
        if (pos == -1) {
          this.renderer.setElementStyle(this.el.nativeElement, 'display', 'none');
        }
      });
    }
  }
}
