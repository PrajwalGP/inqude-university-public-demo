import { Component, ViewChild } from '@angular/core';
import { AngularFire } from 'angularfire2';
import { AlertController, App, Events, IonicApp, LoadingController, MenuController, Nav, Platform, ToastController } from 'ionic-angular';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';

import { LoginPage } from '../pages/login/login';
import { UserData } from '../providers/user-data';
import { UserList } from '../pages/users/user-list';
import { EventListPage } from '../pages/event-list/event-list';
import { EventDetailPage } from '../pages/event-detail/event-detail';
import { NotificationsListPage } from '../pages/notifications/notification-list/notification-list';
import { EditModerator } from '../components/moderator/edit-moderator';
import { MenuPage } from '../pages/menu/menu';
import { PagesList } from '../pages/pages/page-list/page-list';
import { FeedbackRatingListPage } from '../pages/feedbacks-ratings/feedback-rating-list/feedback-rating-list';
import { PostsPage } from '../pages/posts/posts';
import { ModeratorList } from '../pages/moderator-list/moderator-list';
import { NavigationDashboardPage } from '../pages/navigation-dashboard/navigation-dashboard';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { SpeakerListPage } from '../pages/speaker/speaker-list/speaker-list'

import { Device } from '@ionic-native/device';
import { Splashscreen } from 'ionic-native';
import { Storage } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar';
import { CodePush } from '@ionic-native/code-push';
import { Push, PushObject } from '@ionic-native/push';
import { Talisma } from '../providers/crm/talisma';
import { Observable } from 'rxjs/Rx';
import { HelperService } from '../providers/helperService';
import * as $ from 'jquery';
import * as _ from 'underscore';
declare var cordova: any;

export interface PageInterface {
  title: string;
  component: any;
  icon: string;
  logsOut?: boolean
  hideFor?: number;
  index?: number;
  showForWeb?: boolean;
  resetData?: boolean;
}

@Component({
  templateUrl: 'app.template.html',
})

export class BarcodeScan {
  // the root nav is a child of the root app component
  // @ViewChild(Nav) gets a reference to the app's root nav
  @ViewChild(Nav) nav: Nav;

  // List of pages that can be navigated to from the left menu
  // the left menu only works after login
  // the login page disables the left menu

  loggedInPages: PageInterface[] = [
    { title: 'Home', component: NavigationDashboardPage, icon: 'home' },
    { title: 'Events', component: EventListPage, icon: 'calendar' },
    { title: 'Account', component: EditModerator, icon: 'contact' },
    { title: 'Reset data', component: LoginPage, icon: 'refresh', resetData: true },
    { title: 'Logout', component: LoginPage, icon: 'log-out', logsOut: true }
  ];
  loggedInModeratorPages = [
    { title: 'Home', component: NavigationDashboardPage, icon: 'home' },
    { title: 'Events', component: ModeratorList, icon: 'calendar' },
    { title: 'Account', component: EditModerator, icon: 'contact' },
    { title: 'Logout', component: LoginPage, icon: 'log-out', logsOut: true }
  ];
  loggedOutPages: PageInterface[] = [
    { title: 'Login', component: LoginPage, icon: 'log-in' },
  ];

  adminPages: PageInterface[] = [
    { title: 'Moderators', component: UserList, icon: 'contacts', hideFor: 20 },
    { title: 'Personas', component: SpeakerListPage, icon: 'person', hideFor: 20 },
    { title: 'Notifications', component: NotificationsListPage, icon: 'notifications' },
    { title: 'Feedbacks', component: FeedbackRatingListPage, icon: 'chatbubbles', hideFor: 99 },
    { title: 'Pages', component: PagesList, icon: 'document', hideFor: 20 },
    { title: 'Menu', component: MenuPage, icon: 'hammer', hideFor: 20 },
    { title: 'Posts', component: PostsPage, icon: 'md-paper', hideFor: 20 },
  ];

  accountPages: PageInterface[] = this.loggedOutPages;
  rootPage: any;
  loading: any;
  roleValue: any;
  user: any = null;
  isCordovaEnabled: boolean;
  lastTimeBackPress = 0;
  timePeriodToExit: number = 3000;
  showMenu: boolean = false;
  hasLoggedIn: boolean = false;
  notifications: any[] = [];
  userSubsciption: any;

  constructor(
    public af: AngularFire,
    public events: Events,
    public afoDatabase: AngularFireOfflineDatabase,
    public alertCtrl: AlertController,
    public app: App,
    private codePush: CodePush,
    public device: Device,
    public helper: HelperService,
    public ionicApp: IonicApp,
    public loadingCtrl: LoadingController,
    public menu: MenuController,
    public platform: Platform,
    private push: Push,
    private statusBar: StatusBar,
    public storage: Storage,
    public talisma: Talisma,
    public toastCtrl: ToastController,
    public userData: UserData,
  ) {
    let self: any = this;
    this.loading = loadingCtrl.create({
      content: 'Loading ...'
    })
    this.loading.present();

    this.isCordovaEnabled = (this.device.cordova !== null);

    this.storage.ready()
      .then(() => {
        this.statusBar.backgroundColorByHexString('#488aff');
        return new Promise((resolve) => {
          this.storage.get('hasLoggedIn')
            .then((hasLoggedIn) => {
              if (hasLoggedIn) {
                this.getUser();
                this.hasLoggedIn = hasLoggedIn;
                this.helper.getRoleValue()
                  .then((roleValue) => {
                    if (this.hasLoggedIn) {
                      self.showMenu = true;
                      self.rootPage = NavigationDashboardPage;
                      resolve();
                    }
                    else {
                      this.showMenu = false;
                      this.rootPage = LoginPage;
                      resolve();
                    }
                  })
              }
              else {
                this.storage.get('hasSeenTutorial')
                  .then((hasSeenTutorial) => {
                    if (hasSeenTutorial) {
                      this.statusBar.backgroundColorByHexString('#488aff');
                      this.rootPage = LoginPage;
                      resolve();
                    } else {
                      this.rootPage = TutorialPage;
                      resolve();
                    }
                  })
              };
            });
        })
      })
      .then(() => {
        self.platformReady()
          .then(() => {
            self.loading.dismiss();
            self.nav.setRoot(self.rootPage);

            if (self.isCordovaEnabled) {
              self.codepushSync();
              //Executes at every 30 minutes interval
              Observable.interval(1000 * 60 * 30).subscribe(x => {
                self.codepushSync();
              });
            }
          });
      })
      .catch((err) => {
        console.log('err occured' + err);
      })

    // decide which menu items should be hidden by current login status stored in local storage
    this.userData.hasLoggedIn().then((hasLoggedIn) => {
      this.enableMenu(hasLoggedIn === true);
      this.getUser();
    });

    this.listenToLoginEvents();
  }

  changePicture() {
    $('#attachProfilePicture').click();
  }

  saveUrl(pictureObj: any) {
    this.helper.saveProfilePic(pictureObj.url);
  }

  changeRoleValue() {
    this.roleValue = this.roleValue == 99 ? 20 : 99;
    this.userData.setRoleValue(this.roleValue)
      .then(() => {
        this.enableMenu(true);
        this.nav.setRoot(NavigationDashboardPage);
        this.menu.close();
        this.helper.showMessage('Your role has changed to ' + (this.roleValue == 99 ? 'Admin' : 'Event Moderator'), 3000);
      })
  }

  codepushSync() {
    //Codpush checks for new updates & downloads if any new updates are available
    this.codePush.sync()
      .subscribe((syncStatus: any) => {
        console.log('syncStatus', syncStatus)
        if (syncStatus == 1) {
          let alert = this.alertCtrl.create({
            title: 'Restart app',
            message: 'New Updates Installed & requires app to be restarted',
            buttons: [
              {
                text: 'Restart',
                handler: () => {
                  this.codePush.restartApplication();
                }
              }
            ],
            enableBackdropDismiss: false
          });
          alert.present();
        }
      })
  }

  getUser() {
    if (this.user !== null) {
      return false;
    }
    this.userData.getUser()
      .then((userObservable: any) => {
        this.userSubsciption = userObservable
          .subscribe((user: any) => {
            if (!user.$exists()) {
              return false;
            }
            this.user = user;
            this.storage.get('roleValue')
              .then((roleValue: any) => {
                if (this.user.roleValue && this.user.roleValue !== roleValue) {
                  this.userData.setRoleValue(this.user.roleValue)
                    .then(() => {
                      this.userData.hasLoggedIn()
                        .then((hasLoggedIn: any) => {
                          this.enableMenu(hasLoggedIn === true);
                        })
                    })
                }
              })
            if (user.disabled == true) {
              this.logout();
              this.toastCtrl.create({
                message: 'User has been blocked by the Admin, please contact Admin for details',
                duration: 3000
              }).present();
            }
          });
      });
  }

  logout() {
    setTimeout(() => {
      let uid: any;
      this.storage.get('uid')
        .then((res) => {
          uid = res;
          return this.af.auth.logout()
        })
        .then(() => {
          return new Promise((resolve, reject) => {
            if (this.userSubsciption) {
              this.userSubsciption.unsubscribe();
            }
            if (!this.platform.is('core')) {
              this.af.database.object('/notificationTokens/' + uid + '/admin')
                .remove()
                .then(() => {
                  resolve();
                })
                .catch((err) => {
                  reject(err);
                });
            } else {
              resolve();
            }
          })
        })
        .then(() => {
          this.userData.logout();
          this.showMenu = false;
          this.nav.setRoot(LoginPage);
        })
        .catch((err) => {
          console.log('err occured', err);
        })
    }, 100);
  }

  openPage(page: PageInterface) {
    if (page.logsOut === true) {
      // Give the menu time to close before changing to logged out
      this.logout();
      return false;
    }
    if (page.resetData === true) {
      // Give the menu time to close before changing to logged out
      this.resetData();
      return false;
    }
    // the nav component was found using @ViewChild(Nav)
    // reset the nav to remove previous pages and only have this page
    // we wouldn't want the back button to show in this scenario
    if (page.index) {

      this.nav.setRoot(page.component, { tabIndex: page.index });
    } else {
      this.nav.setRoot(page.component).catch(() => {
        console.log("Didn't set nav root");
      });
    }
  }

  listenToLoginEvents() {
    this.events.subscribe('user:login', () => {
      this.showMenu = true;
      this.enableMenu(true);
      this.getUser();
    });

    this.events.subscribe('user:logout', () => {
      this.showMenu = false;
      this.enableMenu(false);
      this.user = null;
    });
  }

  resetData() {
    this.helper.showLoading();
    this.userData.getUniversityId()
      .then((universityId) => {
        return this.helper.dumpUniversityData(universityId);
      })
      .then(() => {
        this.af.database.object('users/' + this.user.uid + '/participationStatus')
          .remove();
      })
      .then(() => {
        this.helper.hideLoading();
        this.helper.showMessage('Data reset successful');
      })
      .catch((err) => {
        console.log('err occured', err);
      })
  }
  enableMenu(loggedIn: boolean) {
    this.storage.get('roleValue')
      .then((roleValue: any) => {
        this.roleValue = roleValue;
        if (loggedIn) {
          if (this.roleValue == 99) {
            this.accountPages = this.loggedInPages;
          }
          else {
            this.accountPages = this.loggedInModeratorPages;
          }
        } else {
          this.accountPages = this.loggedOutPages;
        }
      })
  }

  platformReady() {
    return new Promise((resolve, reject) => {
      let self: any = this;
      this.platform.ready()
        .then(() => {
          if (!this.platform.is('core') && (this.platform.is('android') || this.platform.is('ios'))) {
            this.push.hasPermission()
              .then((res: any) => {
                if (res.isEnabled) {
                  console.log('We have permission to send push notifications');
                } else {
                  console.log('We do not have permission to send push notifications');
                }
              });
            self.afoDatabase.object('settings/icon')
              .take(1)
              .subscribe((icon: any) => {
                let options = {
                  android: {
                    icon: icon.name,
                    iconColor: icon.barcodeAppColor,
                    senderID: '1035472941987'
                  },
                  ios: {
                    senderID: '1035472941987',
                    gcmSandbox: true,
                    alert: 'true',
                    badge: true,
                    sound: 'false'
                  },
                  windows: {},
                  browser: {
                    pushServiceURL: 'http://push.api.phonegap.com/v1/push'
                  }
                };
                let pushObject: PushObject = self.push.init(options);

                pushObject.on('notification').subscribe((notification: any) => {
                  console.log('Received a notification', notification);
                  let data = notification.additionalData;
                  console.log('additionalData', notification.additionalData);
                  if (!data.foreground) {
                    //when the notification is tapped in the notification tray
                    //route to the Specific pages
                    data.route = typeof (data.routes) == 'string' ? JSON.parse(data.routes) : data.routes;
                    if (data.route) {
                      if (data.route === 'NotificationsListPage') {
                        this.nav.setRoot(NotificationsListPage);
                      }
                      else {
                        // this.linkService.viewPage(data.route);
                      }
                    }
                    // for IOS Build uncomment the below & comment the above
                    // data.routes = typeof (data['gcm.notification.routes']) == 'string' ? JSON.parse(data['gcm.notification.route']) : data['gcm.notification.route'];
                    // if (data.route) {
                    //   if (data.route === 'ChatViewPage') {
                    //     this.nav.setRoot(UserListPage).then(() => {
                    //       this.nav.push(ChatViewPage, { uid: data['gcm.uid'], interlocutor: data['gcm.interlocutor'] })
                    //     })
                    //   }
                    //   else if (data.route === 'GroupChatsPage') {
                    //     this.nav.setRoot(UserListPage).then(() => {
                    //       this.nav.push(GroupChatsPage, { uid: data['gcm.uid'], groupId: data['gcm.groupId'] })
                    //     })
                    //   }
                    //   else if (data.route === 'UserListPage') {
                    //     this.nav.setRoot(UserListPage);
                    //   }
                    //   else if (data.route === 'NotificationsListPage') {
                    //     this.nav.setRoot(NotificationsListPage);
                    //   }
                    //   else {
                    //     this.linkService.viewPage(data.route);
                    //   }
                    // }
                  }
                  data.event = data.event ? data.event : {};
                  if (data.target == 'Event Notification' && data.event.eventId) {
                    this.helper.showEventAlert(notification, data.enableEventView)
                      .then((res: any) => {
                        if (res.status != 'DISMISS' && data.event.eventId) {
                          this.helper.showLoading();
                          this.talisma.events.getEventById(data.event.eventId)
                            .then((res: any) => {
                              let event = res[0];
                              this.helper.hideLoading();
                              this.nav.setPages([{ page: ModeratorList, params: {} }, { page: EventDetailPage, params: { event: event } }])
                            })
                            .catch((err: any) => {
                              console.log(err);
                              this.helper.hideLoading();
                            })
                        }
                      });
                  }
                });
                pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
                pushObject.on('registration')
                  .subscribe((registration: any) => {
                    console.log('Device registered', registration);
                    self.userData.hasLoggedIn()
                      .then((hasLoggedIn: any) => {
                        if (hasLoggedIn) {
                          self.userData.getUid()
                            .then((uid: any) => {
                              self.af.database.object('/notificationTokens/' + uid + '/admin')
                                .set({ 'token': registration.registrationId, type: ((this.platform.is('android') && !this.platform.is('core')) ? 'android' : 'ios') });
                              this.userData.setFCMToken(registration.registrationId);
                            })
                        }
                        else {
                          this.userData.setFCMToken(registration.registrationId);
                        }
                      })
                  })
                if (!this.platform.is('core')) {
                  //sets the FCM token to DB once the user loggedIn
                  this.af.auth.subscribe((auth) => {
                    if (auth) {
                      let uid = auth.uid;
                      this.userData.getFCMToken()
                        .then((token) => {
                          if (token) {
                            this.af.database.object('/notificationTokens/' + uid + '/admin')
                              .set({ 'token': token, type: ((this.platform.is('android') && !this.platform.is('core')) ? 'android' : 'ios') });
                          }
                        })
                    }
                  })
                }
              });

            this.platform.registerBackButtonAction(() => {
              let backDrop: any = document.getElementsByTagName('ion-picker-cmp');
              if (backDrop.length > 0) {
                let count = 0;
                for (let i = 0; i < backDrop.length; i++) {
                  if (backDrop[i].style.display !== 'none') {
                    backDrop[i].style.display = 'none';
                    count ++;
                  }
                }
                if(count > 0){
                  return;
                }
              }

              let activePortal = this.ionicApp._loadingPortal.getActive() ||
                this.ionicApp._modalPortal.getActive() ||
                this.ionicApp._toastPortal.getActive() ||
                this.ionicApp._overlayPortal.getActive();

              if (activePortal) {
                activePortal.dismiss();
                return;
              }
              if (this.menu.isOpen()) {
                this.menu.close();
                return;
              }

              if (this.nav.getActive() && ((this.nav.getActive().pageRef().nativeElement.tagName !== 'PAGE-NAVIGATION-DASHBOARD') && this.nav.canGoBack()) && this.nav.getActive().pageRef().nativeElement.tagName !== 'PAGE-LOGIN') {
                this.nav.pop();
                return;
              }
              if (this.nav.getActive() && ((this.nav.getActive().pageRef().nativeElement.tagName !== 'PAGE-NAVIGATION-DASHBOARD') && (this.nav.getActive().pageRef().nativeElement.tagName !== 'PAGE-LOGIN') && !this.nav.canGoBack())) {
                this.storage.get('hasLoggedIn')
                  .then((hasLoggedIn) => {
                    if (hasLoggedIn) {
                      this.showMenu = true;
                      this.nav.setRoot(NavigationDashboardPage);
                    } else {
                      this.showMenu = false;
                      this.nav.setRoot(LoginPage);
                      return;
                    }
                  })
              }
              if (this.nav.getActive() && this.nav.getActive().pageRef().nativeElement.tagName == 'PAGE-NAVIGATION-DASHBOARD' || this.nav.getActive().pageRef().nativeElement.tagName == 'PAGE-LOGIN') {
                if ((+new Date()) - this.lastTimeBackPress < this.timePeriodToExit) {
                  this.platform.exitApp();
                }
                else {
                  this.toastCtrl.create({
                    message: 'Press back again to exit App',
                    duration: 3000,
                    position: 'bottom'
                  }).present();
                  this.lastTimeBackPress = +(new Date());
                }
              }
            })
          }

        })
      Splashscreen.hide();
      resolve();
    });
  }

  isActive(menuItem: any) {
    let activeComponent = this.nav.getActive();
    if (!activeComponent || !activeComponent.component) {
      return '';
    }
    let isComponentPage: boolean = activeComponent.component.name && activeComponent.component.name === menuItem.value;
    let isCustomePage: boolean = activeComponent.component.name && activeComponent.component.name === 'CustomPage' && activeComponent.data && activeComponent.data.pageId === menuItem.value;

    if (isComponentPage || isCustomePage) {
      return 'primary';
    }
    return '';
  }
}
