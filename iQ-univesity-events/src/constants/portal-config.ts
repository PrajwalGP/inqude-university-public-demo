
export class PortalConfiguration {

  ENV: any = 'test';
  servers: any = {
    'local': 'assets/html/login.html',
    'test': 'https://ccsnoncredittest.ua.edu/CustomerRegistration/Account/',
    'production': ''

  };
  constructor() {  }

  public getServerUrl() {
    return this.servers[this.ENV];
  }

  public getLoginUrl() {
    return this.getServerUrl() + 'Login?source=eventapp';
  }
}
