import { Component, EventEmitter, Input, Output } from '@angular/core';
import { LoadingController, Platform, ToastController } from 'ionic-angular';
import { HelperService } from '../../providers/helperService';
import { AngularFire } from 'angularfire2';
import * as firebase from 'firebase';
import { UtilProvider } from '../../providers/util-provider';
import * as moment from 'moment';
import { UserData } from '../../providers/user-data';

@Component({
  selector: 'attach-files',
  templateUrl: 'attach-files.html'
})

export class AttachFilesComponent {
  file: any = {};
  isWebPlarform: boolean = true;
  @Input('params') params: any;
  @Output() saveUrl = new EventEmitter();
  @Output() autoSelectFile = new EventEmitter();
  uploadType: string;
  path: string;
  storageLocation: any;
  folder: any;
  acceptedFileTypes: any;
  fileIcons: any;
  universityId: string = '';

  constructor(
    public af: AngularFire,
    public loadingCtrl: LoadingController,
    public helper: HelperService,
    public platform: Platform,
    public toastCtrl: ToastController,
    public userData: UserData,
    public utilProvider: UtilProvider
  ) {
    if (this.platform.is('android') || this.platform.is('ios')) {
      this.isWebPlarform = false;
    }
    this.userData.getUniversityId()
      .then((universityId) => {
        this.universityId = universityId;
        this.af.database.object("/settings/fileTypes", { preserveSnapshot: true })
          .take(1)
          .subscribe(settings => {
            this.uploadType = this.params.uploadType;
            this.acceptedFileTypes = this.uploadType == "file" ? settings.val() : "image/*";
            this.path = this.uploadType == "file" ? "files" : "images";
          })
      })

  }

  ngOnInit() {
    this.params.domId = this.params.domId || 'image';
  }

  onChange(event: any) {
    let eventObj: MSInputMethodContext = <MSInputMethodContext>event;
    let target: HTMLInputElement = <HTMLInputElement>eventObj.target;
    let files: FileList = target.files;
    if (files.length > 0) {
      this.file = files[0];
      this.uploadFile();
    }
  }

  uploadFile() {
    let loader: any;
    if (!this.params.returnUrl) {
      loader = this.loadingCtrl.create({
        content: (this.uploadType == "file" ? "File" : "Image") + " is uploading... please wait",
      });
      loader.present();
    }
    this.helper.generateUUID()
      .then((data: any) => {
        this.params.fileName = this.params.enableUUID != true && this.uploadType != 'file' ? moment.utc().format('x') : undefined;
        if (this.params.fileName == undefined) {
          this.params.fileName = data.uuid;
        }
        this.uploadToStorage()
          .then((snapshot: any) => {
            if (this.params.returnUrl) {
              this.saveUrl.emit({ url: snapshot.downloadURL });
            } else {
              this.linkToDb(snapshot.downloadURL);
              loader.dismiss();
            }
          })
      })
  }

  uploadFromDevice() {
    let image: string;
    this.utilProvider.getPicture()
      .then((_image: string) => {
        image = _image;
        return this.helper.generateUUID();
      })
      .then((data: any) => {
        if (this.params.fileName == undefined) {
          this.params.fileName = data.uuid;
        }
        return this.utilProvider.savePicture(this.params.fileName, (this.universityId + '/' + this.params.folder), image);
      })
      .then((downloadUrl: string) => {
        if (this.params.returnUrl) {
          this.saveUrl.emit({ url: downloadUrl });
        } else {
          this.linkToDb(downloadUrl);
        }
      })
      .catch((e: any) => { });
  }

  uploadToStorage() {
    this.folder = this.params.folder == 'schedule' && this.uploadType != 'file' ? (this.universityId + '/schedule') : (this.universityId + '/' + this.params.folder);
    this.storageLocation = this.uploadType == "file" ? (this.universityId + '/' + this.path) : this.folder;
    const storageRef = firebase.storage().ref('')
      .child(this.storageLocation)
      .child(this.uploadType == 'file' ? this.params.fileName + '.' + this.getFileExtension() : this.params.fileName);
    return storageRef.put(this.file);
  }

  linkToDb(downloadUrl: any) {
    this.uploadType != "file" ? this.linkImageToDb(downloadUrl) : this.linkFileToDb(downloadUrl);
  }

  linkImageToDb(downloadUrl: any) {
    this.af.database.object('universitiesData/' + this.universityId + '/' + this.path + '/' + this.params.folder + '/' + this.params.fileName)
      .set(downloadUrl)
      .then(() => {
        this.autoSelectFile.emit({ data: downloadUrl });
        let toast = this.toastCtrl.create({
          message: 'Image uploaded successfully',
          duration: 3000
        });
        this.params.fileName = undefined;
        toast.present();
      })
      .catch(error => {
        console.log(error);
      });
  }

  getMetaData() {
    let self = this;
    return new Promise((resolve, reject) => {
      const storageRef = firebase.storage().ref().child(this.storageLocation).child(this.params.fileName + '.' + this.getFileExtension())
      storageRef.getMetadata()
        .then(function(metadata) {
          let metaDataObj = {
            size: metadata.size,
            id: self.params.fileName,
            fileName: self.file.name,
            fileUrl: metadata.downloadURLs[0],
            fileType: metadata.contentType,
            modifiedTime: metadata.updated
          };
          resolve(metaDataObj);
        })
        .catch(function(error) {
          console.log("File Metadata not found", error);
        });
    });
  }

  getFileExtension() {
    return this.file ? this.file.name.substr(this.file.name.lastIndexOf('.') + 1) : '';
  }

  linkFileToDb(downloadUrl: any) {
    let self = this;
    this.getMetaData()
      .then(function(metadata) {
        self.af.database.object('universitiesData/' + self.universityId + '/' + self.path + '/' + self.params.folder + '/' + self.params.fileName)
          .set(metadata)
          .then(() => {
            self.autoSelectFile.emit({ data: metadata });
            let toast = self.toastCtrl.create({
              message: 'File uploaded successfully',
              duration: 3000
            });
            self.params.fileName = undefined;
            toast.present();
          })
          .catch(error => {
            console.log(error);
          });
      });
  }
}
