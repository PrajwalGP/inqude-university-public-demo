import { Component, Input } from "@angular/core";
import * as moment from "moment-timezone";
import { ModalController, NavController } from "ionic-angular";
import { EventDetailPage } from "../../pages/event-detail/event-detail";
import { EventTicket } from "../event-ticket/event-ticket";
import { Talisma } from "../../providers/crm/talisma";
import { UserData } from "../../providers/user-data";
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';

@Component({
  selector: "single-event-new",
  templateUrl: "single-event-new.html"
})
export class SingleEventNew {
  @Input("params") event: any;
  @Input("hideTicket") hideTicket: boolean;
  moment = moment;
  participationStatus: any;
  uid: string;
  timeZone:string;

  constructor(
    public afoDatabase:AngularFireOfflineDatabase,
    public modalCtrl: ModalController,
    private navCtrl: NavController,
    private talisma: Talisma,
    private userData: UserData) { }

  ngOnChanges() {
    this.userData
      .getUid()
      .then((uid) => {
        this.uid = uid;
        return this.talisma.participants.getFirebaseParticipationStatus(this.event.EventId, this.uid);
      })
      .then((result: any) => {
        if (result && result.$exists()) {
          this.participationStatus = result.status;
        }
        this.afoDatabase
          .object("settings/timeZone")
          .take(1)
          .subscribe((timeZone: any) => {
            this.timeZone = timeZone.$exists() ? timeZone.$value : '';
          });
      });
  }

  viewEvent(eventData: any) {
    this.navCtrl.push(EventDetailPage, { event: eventData });
  }

  viewTicket(event: any, eventData: any) {
    event.stopPropagation();
    if (!this.hideTicket && (this.participationStatus == "Registered" || this.participationStatus == "Attended")) {
      let modal = this.modalCtrl.create(EventTicket, { eventId: eventData.EventId, timeZone:this.timeZone }, { enableBackdropDismiss: true });
      modal.present();
    }
  }

  getParticipationStatusDotClass(status: string) {
    let className: any
    switch (status) {
      case 'Registered': {
        className = 'registered-status-dot'
        break;
      }
      case 'Attended': {
        className = 'attended-status-dot'
        break;
      }
      case 'Interested': {
        className = 'interested-status-dot'
        break;
      }
      case 'Cancelled': {
        className = 'cancelled-status-dot'
        break;
      }
    }
    return className;
  }
}
