import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";
import { AngularFire } from "angularfire2";
import { App, Events, NavParams, NavController, MenuController, LoadingController, ToastController, ViewController, Platform } from "ionic-angular";
import { HelperService } from "../../services/services";
// import { UserDetailFormPage } from "../users/user-detail-form/user-detail-form";
import { UserData } from "../../providers/user-data";
import { NavigationDashboardPage } from "../navigation-dashboard/navigation-dashboard";
import * as firebase from "firebase";
declare let window: any;
import { Keyboard } from "@ionic-native/keyboard";

@Component({
  selector: "page-otp",
  templateUrl: "otp.html",
  providers: [HelperService]
})
export class OtpPage {
  user: { otp?: any; generatedOtp?: any; email?: any; roleValue?: number; username?: string; uid: string };
  submitted = false;
  redirectPage?: string;
  otpExpired: boolean = false;
  showFooter: boolean = true;

  constructor(
    public app: App,
    public af: AngularFire,
    public events: Events,
    public navParams: NavParams,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public helperService: HelperService,
    public keyboard: Keyboard,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public userData: UserData,
    public viewCtrl: ViewController,
    public platform: Platform
  ) {
    this.user = this.navParams.get("user");

    // hide footer on keyboard show
    this.keyboard.onKeyboardShow().subscribe(() => {
      this.showFooter = false;
    });

    // show footer on keyboard hide
    this.keyboard.onKeyboardHide().subscribe(() => {
      this.showFooter = true;
    });
  }

  validateOtp(form: NgForm) {
    this.submitted = true;
    let self: any = this;
    if (form.valid) {
      let loading = this.loadingCtrl.create({
        content: "Validating OTP, Please wait"
      });
      loading.present();
      if (this.user.otp == this.user.generatedOtp) {
        this.helperService
          .createToken(this.user.uid, this.user.email)
          .then((res) => {
            this.user = res.user;
            return firebase.auth().signInWithCustomToken(res.token);
          })
          .then((firebaseUser) => {
            if (self.user.isNewUser || self.user.registrationComplete == undefined) {
              loading.dismiss();
              // self.navCtrl.setRoot(UserDetailFormPage, { uid: this.user.uid, roleValue: self.user.roleValue });
            } else {
              self.userData
                .login(self.user)
                .then(() => {
                  this.events.publish("user:login");
                  loading.dismiss();
                  this.menuCtrl.enable(true);
                  return this.app.getRootNav().setRoot(NavigationDashboardPage);
                })
                .catch((err: any) => {
                  loading.dismiss();
                  console.log("error occured", err);
                });
            }
          })
          .catch((error: any) => {
            let msg = "Authentication failed:" + error;
            loading.dismiss();
            self.toastCtrl
              .create({
                message: msg,
                duration: 3000,
                position: "top"
              })
              .present();
          });
      } else {
        loading.dismiss();
        let toast = this.toastCtrl.create({
          message: "Invalid OTP, enter a valid OTP",
          duration: 3000
        });
        toast.present();
      }
    }
  }

  resendOtp() {
    let loading = this.loadingCtrl.create();
    loading.present();
    this.helperService
      .sendOtp(this.user.email, "VerifyAccount")
      .then((resp) => {
        if (resp.msg == "success") {
          this.user.generatedOtp = resp.otp;
          loading.dismiss();
          let toast = this.toastCtrl.create({
            message: "OTP sent successfully",
            duration: 1500
          });
          toast.present();
        } else {
          loading.dismiss();
          let toast = this.toastCtrl.create({
            message: "Failed to send the OTP, please try after some time",
            duration: 1500
          });
          toast.present();
        }
      })
      .catch((err) => {
        loading.dismiss();
        console.log("failed to send a otp", err);
        let toast = this.toastCtrl.create({
          message: "Failed to send the OTP, please try after some time",
          duration: 1500
        });
        toast.present();
      });
  }
}
