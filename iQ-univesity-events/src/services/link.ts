import { Injectable, Inject } from '@angular/core';
import { App } from 'ionic-angular/index';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { HelperService } from './services';
import { HomePage } from '../pages/home/home';
import { SchedulePage } from '../pages/schedule/schedule';
import { SponsorPage } from '../pages/sponsors/sponsor-list/sponsor-list';
import { NavigationDashboardPage } from '../pages/navigation-dashboard/navigation-dashboard';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
import { GroupChatsPage } from '../pages/chats/group-chats/group-chats';
import { ChatViewPage } from '../pages/chats/chat-view/chat-view';
import { HospitalityPage } from '../pages/hospitality/hospitality';
import { UserListPage } from '../pages/users/user-list/user-list';
import { InAppBrowser } from 'ionic-native';
import { CustomPage } from '../pages/custom-page/custom-page';
import { ScheduleListingPage } from '../pages/schedule-listing/schedule-listing';
import { SpeakerListPage } from '../pages/speaker/speaker-list/speaker-list';
import { MapPage } from '../pages/map/map';
import { SessionDetailPage } from '../pages/session/session-detail/session-detail';

@Injectable()
export class LinkService {
  components: any = {};

  constructor(
    @Inject(HelperService) public helper: HelperService,
    public afoDatabase: AngularFireOfflineDatabase,
    public app: App
  ) {
    this.components = {
      HomePage: HomePage,
      SponsorPage: SponsorPage,
      NavigationDashboardPage: NavigationDashboardPage,
      AccountPage: AccountPage,
      LoginPage: LoginPage,
      SchedulePage: SchedulePage,
      HospitalityPage: HospitalityPage,
      UserListPage: UserListPage,
      ChatViewPage: ChatViewPage,
      GroupChatsPage: GroupChatsPage,
      ScheduleListing: ScheduleListingPage,
      SpeakerListPage: SpeakerListPage,
      MapPage: MapPage
    };
  }

  // Navigate to Page or External url
  openLink(data: any) {
    return new Promise((resolve) => {

      if (data.type == 'PAGE') {
        this.app.getRootNav().setRoot(CustomPage, { pageId: data.value, title: data.title });
      } else if (data.type == 'LAUNCH_URL') {
        if (data.value.indexOf('http') < 0) {
          data.value = 'http://' + data.value;
        }
        new InAppBrowser(data.value, '_blank');
      }
      resolve();
    });
  }

  viewPage(menu: any) {
    if (menu.type == 'COMPONENT') {
      this.app.getRootNav().setRoot(this.components[menu.value], { title: menu.title });
    }
    else if (menu.type == 'SCHEDULE') {
      this.app.getRootNav().setPages([{ page: NavigationDashboardPage }, { page: SchedulePage }, { page: SessionDetailPage, params: { title: menu.title, sessionId: menu.value } }])
    }
    else {
      this.openLink(menu);
    }
  }
}
