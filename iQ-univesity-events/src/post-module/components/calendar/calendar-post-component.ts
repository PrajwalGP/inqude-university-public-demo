import { Component, Input } from '@angular/core';
import { PostComponent } from '../../post.component';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { Events, NavController } from 'ionic-angular';
import { HelperService } from '../../../providers/helperService';

@Component({
  selector: 'calendar-post-component',
  templateUrl: 'calendar-post-component.html'
})

export class CalendarPostComponent implements PostComponent {
  @Input() post: any = { session: {} };
  @Input() params: any;
  isReady: boolean = false;

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public helperService: HelperService,
    public navCtrl: NavController,
    public events: Events
  ) { }

  ngOnInit() {
    this.helperService.getUniversityId()
      .then((universityId) => {
        if (this.post && this.post.sessionId) {
          this.afoDatabase.object('universitiesData/' + universityId + '/events/' + this.post.sessionId)
            .subscribe((session) => {
              if (session.$exists()) {
                this.isReady = true;
                this.post.session = session;
              } else {
                this.events.publish('delete:post', this.post.id);
              }
            })
        } else {
          this.events.publish('delete:post', this.post.id);
        }
      });
  }
}
