import { Component, Input } from '@angular/core';

import { PostComponent } from '../../post.component';

@Component({
  templateUrl: 'youtube-post.component.html',
  selector: 'youtube-post-component'
})
export class YouTubePostComponent implements PostComponent {
  @Input() post: any;
  @Input() params: any;
  youtubeThumbnail: string = '';

  ngOnInit() {
    if (this.post && this.post.youtubeUrl && this.post.youtubeUrl.split("v=")[1]) {
      this.youtubeThumbnail = 'https://img.youtube.com/vi/' + this.post.youtubeUrl.split("v=")[1] + '/mqdefault.jpg';
    }
  }
}
