import { Headers } from '@angular/http';

export class ApiConfigurations {

  ENV: any = 'firebase';
  servers: any = {
    'firebase':' https://us-central1-eventiplex-public.cloudfunctions.net/',
    'test': 'http://em-tal-ws8.em.ua-net.ua.edu/',
    'publicTest': 'http://crmtest.ua.edu/',
    'local': '/assets/mock-data/',
  };
  constructor() {  }

  getHeaders() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    // headers.append('Username', 'talismaadmin');
    // headers.append('Username', 'talismaadmin');
    // headers.append('Access-Control-Allow-Credentials', 'true');
    headers.append('Access-Control-Allow-Origin', '*');
    return headers;
  }

  public getEnv() {
    return this.ENV;
  }

  public getServerUrl() {
    return this.servers[this.getEnv()];
  }

}
